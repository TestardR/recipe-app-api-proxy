# 1. Setup repository for NGINX proxy

Date: 2021-07-09

## Status

Accepted

## Context

Our Django application needs a proxy to serve static files.

## Decision

We created this repository so as to implement a NGINX proxy.

## Consequences

Setup project, no risks nor benefits to forcast so far.
